
currentPath = "Y:/Diego/Cell_Stretching_Control_4_04/Tissue_2/";
currentFile = "Capture 4 - 2 Montage_XY0_Z0_T00_C0.tif";

outputPath = "Y:/Diego/Cell_Stretching_Control_4_04/convertToStack/";

xCoord = newArray(2256, 7050, 11850, 1872, 7000, 12100, 2208, 7000, 11800);
yCoord = newArray(2220, 2200, 2180, 7440, 7480, 7390, 12800, 12790, 12790); 

for (i=0; i < 37; i++) {
	open(currentPath + currentFile);
	setOption("ScaleConversions", true);
	run("8-bit");
	for (j = 0; j <= 2; j++) {
		run("Specify...", "width=3100 height=2200 x=" + xCoord[j] +" y=" + yCoord[j] +" centered");
		run("Duplicate...", " ");
		saveAs("Tiff", outputPath + "time_" + i + "_tissue_" + j);
		close();
		selectWindow(currentFile);
	}
	for (j = 3; j <= 5; j++) {
		run("Specify...", "width=2600 height=2600 x=" + xCoord[j] +" y=" + yCoord[j] +" centered");
		run("Duplicate...", " ");
		saveAs("Tiff", outputPath + "time_" + i + "_tissue_" + j);
		close();
		selectWindow(currentFile);
	}
	for (j = 6; j <= 8; j++) {
		run("Specify...", "width=3100 height=2200 x=" + xCoord[j] +" y=" + yCoord[j] +" centered");
		run("Duplicate...", " ");
		saveAs("Tiff", outputPath + "time_" + i + "_tissue_" + j);
		close();
		selectWindow(currentFile);
	}
	
	close();
	if (i < 10) {
		currentFile = "Capture 4 - 2 Montage_XY0_Z0_T0" + i +"_C0.tif";
	} else {
		currentFile = "Capture 4 - 2 Montage_XY0_Z0_T" + i +"_C0.tif";
	}
}

