nImg = 1;


for(i = 0; i < nImg; i++) {
	open("F:/Stretch_Processing/Cropped/ROI_Phase.tif");
	run("Find Edges", "stack");
	run("Gaussian Blur...", "sigma=3 stack");
	setAutoThreshold("Triangle dark");
	//run("Threshold...");
	run("Convert to Mask", "method=Triangle background=Dark calculate black");
	//run("Close");
	run("Analyze Particles...", "size=1000000-Infinity show=Masks display clear stack");
	run("Invert", "stack");
	run("Save", "save=[F:/Stretch_Processing/Mask/Mask of ROI PHASE]");
}

