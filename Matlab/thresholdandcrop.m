%{ 
Relies on functions: MaskSingleTissue, normalizeImage, padImage, trimCrop

This file is for taking raw data from the nikon, letting you crop out single
 tissues of interest from the final tissue and name them, center the tissue 
stack based on the centroid of the starting tissue, and pads the images to 
make them all appropriate for JulieVision. (For the size, the dimensions of 
the output *1.4 should be divisible by 256). user input lets you say
whether you want data for PIV (we scale down by 2 and mask the background),
masks for taking dynamics or using to masks other outputs, output tif
time slices for Julievision,and/or cropped phase stacks

(scaling for JulieVision
needs to be adjusted slightly for the Zeiss output).

%}
%% Inputs, be careful that these are correct
inputFolder='F:\Control_Processing\TestCrop';
%be sure to include the full path of the input folder. It may be from
%latrobe
outputFolder='F:\Control_Processing\Threshholded';
%again, full path needed. may just use the input folder or specify a
%different location. These are where the stacks will go

inputDishNames={'xy1c1'}; %these must uniquely identify each dish separately from the other dish names in the nikon output folder

fluorNames={'c1'}; %do in the order of phase, rfp, gfp. If you have no fluor channels this doesn't matter

outputDishNames={'d2'}; %these will go into the name of the 
%output file. Best practice is to use the same numbers as follow the xy on the raw data

paddedDimension={[3840,3840],[5120,2560],[7680,3840]}; %These are the recommended output file sizes
%for circles, small ellipses, and large ellipses, respectively. You can
%make your own, just remember it is width, height here

baseName='F8';
%This is the part of the name that tells us which experiment it came from

suffix=0; %use a letter for basic naming, use a 0 for more advanced naming 
%(0 recommended), it will ask for user input for the suffix name after you
%crop the given tissue

clipPercentage=.0001;
%sets how we clip data when normalizing the grayscales. .0001 has been
%working fine

if isequal(length(outputDishNames),length(inputDishNames))==0
    error('input,output,and dimension lengths do not match up')
end

%asks input for which outputs you want
inputJV=inputdlg('Do you want images for JulieVision? 1 for yes, 0 for no');
JulieVision=str2double(inputJV{1});
if JulieVision==1
    inputNikon=inputdlg('Is this data from Nikon 4x? (if so, will scale by 1.4 for Julievision) 1 for yes, 0 for no');
    Nikon=str2double(inputNikon{1});
end
inputmasks=inputdlg('Do you want masks for area calculations? 1 for yes, 0 for no');
inputPIV=inputdlg('Do you want PIV tif stacks? 1 for yes, 0 for no');
inputPhase=inputdlg('Do you want phase data? 1 for yes, 0 for no');
inputWhichCentroid=inputdlg('Do you want to center your tissue based on t0 or final? 0 t0, 1 for final (0 usually recommended)');
inputFluor=inputdlg('Do you want to crop fluor channels? 1 for yes, 0 for no');
Fluor=str2double(inputFluor{1});
if Fluor==1
    inputFluorFreq=inputdlg('What is the frequency of Fluor capture? 1 every time point, 2 for every 2, 3 for every 3 etc.');
    fluorFreq=str2double(inputFluorFreq{1});
end

%these just keep track of which images we want to save


whichCentroid=str2double(inputWhichCentroid{1});
MasksForMeasuring=str2double(inputmasks{1});
PIV=str2double(inputPIV{1});
paddedPhase=str2double(inputPhase{1});
%% Cropping

%Loops over dishes and accepts user input for cropping
numberOfTissues=zeros(1,length(inputDishNames));
initialCropBox=cell(length(inputDishNames),1); %collects user input crops
editedCropBox=initialCropBox; %trims user input down to appropriate size
startingCentroids=initialCropBox; %location of centroids of tissue in cropped images
shapeType=initialCropBox;
filenamesList=cell(length(inputDishNames),1); %groups all filenames appropriately
amount=initialCropBox; 
saveList=cell(length(inputDishNames),1);
for d=1:length(inputDishNames)
    %first determine how many tissues we want to crop on this dish
    
    direc = dir([inputFolder,filesep,['*',inputDishNames{d},'*']]); filenames={};
    [filenames{1:length(direc),1}] = deal(direc.name);
    filenamesList{d} = sortrows(filenames); %sort all image files
    amount{d} = length(filenames);
    fprintf('start');
    Ilast=imread([inputFolder,filesep,filenames{end}]);
    resizedIlast=imresize(Ilast,0.2); %scale down for faster visualizing
    gmag=imgradient(resizedIlast); %find edges command to easily see tissue for cropping
    figure();
    imshow(gmag,[]); %displays dish image and accepts input
    inputNumberOfTissues=inputdlg('How many tissues to crop on this dish?');
    close;
    numberOfTissues(d)=str2double(inputNumberOfTissues{1});
    
    %then for each dish, take the crop of each tissue
    for c=1:numberOfTissues(d)  
        [~,thisRect]=imcrop(gmag,[]); %thisRect is the cropping rectangle that was drawn on input
        initialCropBox{d,c}=thisRect*5;
        if suffix==0
            input=inputdlg(['What would you like to make the suffix to this tissue: ', baseName, outputDishNames{d},num2str(d), '_____?']);
            saveList{d,c}=input{1};
        else
            saveList{d,c}=[suffix,num2str(c)];
        end
        inputShapeType=inputdlg('What type of shape is this? (1 for circle, 2 for small ellipse, 3 for large ellipse)');
        shapeType{d,c}=str2double(inputShapeType{1});
        close
    end
end

%loop: trims the input crop to be within the range set by padded dimension
%and records the location of the centroid of the tissue with respect to
%this cropped image

for d=1:length(inputDishNames)
    %Allows to center the tissue based on t0 or tfinal. 
    if whichCentroid==0
        cropImage14bit=imread([inputFolder,filesep,filenamesList{d}{1}]); 
    elseif whichCentroid==1
        cropImage14bit=imread([inputFolder,filesep,filenamesList{d}{1}]); 
    end
    for c=1:numberOfTissues(d) %loop over each tissue on that raw file
            first14bitCropped=imcrop(cropImage14bit,initialCropBox{d,c});
            first8bitcropped=normalizeImage(first14bitCropped,clipPercentage,8);
            firstMask8bit=MaskSingleTissue(first8bitcropped);
            [editedCropBox{d,c},startingCentroids{d,c}]=trimCrop(firstMask8bit,initialCropBox{d,c},paddedDimension{shapeType{d,c}});
    end
end


if JulieVision==1
    mkdir([outputFolder,filesep, baseName,' For JulieVision'])
end

%% Image processing and Saving
%loops over each timepoint for each dish, and then each tissue at that
%dish/timpoint


if MasksForMeasuring==1 ||PIV==1
mkdir([inputFolder,filesep,baseName, 'stacks'])
end
maxnumber=max([amount{:}]);
writeMode=cell(1,maxnumber); %for the first frame, we write a new file, for the rest, we just append on the end of the stack
writeMode(2:end)={'append'};
writeMode(1)={'overwrite'};

for d=1:length(inputDishNames)
    for k=1:amount{d}
        fullImage14bit=imread([inputFolder,filesep,filenamesList{d}{k}]); 
        %this is your raw file from the scope
        for c=1:numberOfTissues(d) %loop over each tissue on that raw file

            
            cropped14bit=imcrop(fullImage14bit,editedCropBox{d,c}); %crops image for this tissue
            cropped8bit=normalizeImage(cropped14bit,clipPercentage,8); %converts to 8bit with no losses
            mask=MaskSingleTissue(cropped8bit); %finds the mask of this tissue (triangle threshold method)

            paddedMask=padImage(mask,startingCentroids{d,c},paddedDimension{shapeType{d,c}}); %finds a mask the appropriate size            
            
            if paddedPhase==1
                paddedCropped8bit=padImage(cropped8bit,startingCentroids{d,c},paddedDimension{shapeType{d,c}}); %pads the cropped 8 bit for saving
                imwrite(paddedCropped8bit,[outputFolder,filesep,baseName,outputDishNames{d},saveList{d,c},'_phase.tif'],'tif','Compression','none','Writemode',writeMode{k})
                %saved the image slice as the appropriate location in the
                %tif stack
            end
            
            if MasksForMeasuring==1
              imwrite(paddedMask,[outputFolder,filesep,baseName,outputDishNames{d},saveList{d,c},'_mask.tif'],'tif','Compression','none','Writemode',writeMode{k})
                %aves the padded mask out
            end
            
            if JulieVision==1
                cropped16bit=normalizeImage(cropped14bit,clipPercentage,16);
                backsub16bit=cropped16bit;
                backsub16bit(mask==0)=0; %removes background determined by the mask
                paddedBacksub16bit=padImage(backsub16bit,startingCentroids{d,c},paddedDimension{shapeType{d,c}});
                %pads your julievision image up to size
                
                %if this is from the nikon, scale up by 1.4. Otherwise just
                %leave it
                if Nikon==1
                    paddedScaledBacksub16bit=imresize(paddedBacksub16bit,1.4);
                    imwrite(paddedScaledBacksub16bit,[outputFolder,filesep, baseName, ' For JulieVision',filesep,baseName,outputDishNames{d},saveList{d,c},'_t',sprintf('%03d',k),'.tif'],'tif','Compression','none')
                else
                    imwrite(paddedBacksub16bit,[outputFolder,filesep, baseName, ' For JulieVision',filesep,baseName, outputDishNames{d},saveList{d,c},'_t',sprintf('%03d',k),'.tif'],'tif','Compression','none')
                end
            end
            
            if PIV==1
                cropped8bit=normalizeImage(cropped14bit,clipPercentage,8);
                scaled8bit=imresize(cropped8bit,0.5);
                %makes mask from larger mask. Scales down and then is
                %careful to fill any holes generated from scaling down and
                %remove any cells that were "freed" from the tissue by
                %scaling down
                scaledDownMask=imresize(mask,0.5);
                scaledMaskSingleTissue=bwareafilt(scaledDownMask,1);
                scaledMask=imfill(scaledMaskSingleTissue,'holes');
                
                
                scaledBacksub8bit=scaled8bit;
                scaledBacksub8bit(scaledMask==0)=0; %removes background
                paddedScaledBacksub8bit=padImage(scaledBacksub8bit,startingCentroids{d,c}/2,paddedDimension{shapeType{d,c}}/2);
                paddedScaledMask=padImage(scaledMask,startingCentroids{d,c}/2,paddedDimension{shapeType{d,c}}/2);
                
                %save the scaled down masks and scaled down images for PIV
                %analysis
                imwrite(paddedScaledMask,[outputFolder,filesep,baseName,outputDishNames{d},saveList{d,c},'_scaledMask.tif'],'tif','Compression','none','WriteMode',writeMode{k})
                imwrite(paddedScaledBacksub8bit,[outputFolder,filesep,baseName,outputDishNames{d},saveList{d,c},'_scaledPhase.tif'],'tif','Compression','none','WriteMode',writeMode{k})         
            end 
        end
    end
end

clearvars -except shapeType directory maskFilenames inputFolder outputFolder inputDishNames OutputDishNames amount...
    paddedDimension BaseName clipPercentage numberOfTissues editedCropBox startingCentroids filenamesList...
    saveList

%save crop settings and names so you don't have to remember what you named
%everything to recrop some things
save([outputFolder,filesep, 'cropSettings.mat']);