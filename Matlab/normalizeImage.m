%{
This function is similar to the process-enhance contrast- normalize
function in imagej. percentPixelsToClip is simply the saturated pixels
value in imageJ.
The function can also be used for easy conversions between bit depths if percentPixelsToCLip is set to 0. 

Inputs are 1. your image you want to change histogram of
            2. number of saturated pixels
            3. output bit depth, but you can only have 8 or 16 for this
            input
%}

function [outputImage]=normalizeImage(inputImage,percentPixelsToClip,output8or16bit)
if percentPixelsToClip >1 || percentPixelsToClip<0
   error('please choose percent of Pixels to clip between 0 and 1 (input 2)'); 
end
imsize=size(inputImage);
reshaped=reshape(inputImage,[imsize(1)*imsize(2),1]);
sorted=sort(reshaped); %now your image is a super long vector with all the gray values in order
amax=double(sorted(round(length(sorted)*(max([1-percentPixelsToClip,percentPixelsToClip])))));
%the value of the minimum pixel that is saturated at the high gray values
%end
amin=double(sorted((round(length(sorted)*(min([1-percentPixelsToClip,percentPixelsToClip]))))+1));
%the value of the maximum pixel that is used for cropping on the minimum
%end
enhancedImage=mat2gray(inputImage,[amin amax]);
%creates an image using the saturation numbers scaled from 0 to 1

if output8or16bit==16

    outputImage=uint16(enhancedImage*(2^16-1)); %rescales to 16 bit
    

elseif output8or16bit==8
    outputImage=uint8(enhancedImage*(2^8-1)); %rescales to 8 bit
else
    error('please choose 8 or 16 for output bit depth (input 3)');
end
end

