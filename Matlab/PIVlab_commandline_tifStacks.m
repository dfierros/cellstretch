% Example script how to use PIVlab from the commandline
% You can adjust the settings in "s" and "p", specify a mask and a region of interest
 %Create list of images inside specified directory
 
    % This script looks in a folder and loops over many tif stacks, looping
    % over each timepoint of each tif stack before moving on to the next
    % tif stack
    %For the input folder, I'm using a naming convention where i uniquely identify the tissue,
    %and follow it with a suffix to say what kind of image it is. 
    %As an example tissue 3 from dish 4 of Experiment 8 might be named
    %E8d4t1_scaledPhase.tif, and the mask would be E8d4t1_scaledMask.tif
 
 
inputName=inputdlg('what would you like to name this data? (leave off the ".mat" file extension)'); 
saveName=[inputName{1} '.mat'];
directory=uigetdir; %directory containing the images you want to analyze


binAmount=1;
pixelSizeMicrons=1.308; %different for the nikon and the zeiss
rollingAverage=1; %if you need to smooth your piv data
framesPerHour=3; %if frame rate of your inputs. These settings allow the output of u,v,vmag, etc to be in microns/hr

%finds all the image files that are your masked phase tif stacks, so they
%must be named in some logical way. I use phase images scaled 2x2 so i call
%use the suffix _scaledPhase
phaseContains='*_phase.tif';
phaseDirec = dir([directory,filesep,phaseContains]); phaseFilenames={};
[phaseFilenames{1:length(phaseDirec),1}] = deal(phaseDirec.name);
phaseFilenames = sortrows(phaseFilenames); %sort all image files

%you also should have the respective mask tif stack for masking at the end
maskContains='*_mask.tif'; %*.bmp or *.tif or *.jpg or *.tiff or *.jpeg
maskDirec=dir([directory,filesep,maskContains]); maskFilenames={};
[maskFilenames{1:length(maskDirec),1}] = deal(maskDirec.name);
maskFilenames = sortrows(maskFilenames); %sort all image files

%here i just check to make sure the folder is all matched up for analysis
for i=1:length(phaseFilenames)
    %I'm using a naming convention where i uniquely identify the tissue,
    %and follow it with a suffix to say what kind of image it is. 
    %As an example tissue 3 from dish 4 of Experiment 8 might be named
    %E8d4t1_scaledPhase.tif, and the mask would be E8d4t1_scaledMask.tif
    phaseToken=strtok(phaseFilenames{i},'_');
    %phase token pulls out the "E8d4t1" part, which should be the same on
    %the mask and phase file of the same image
    maskToken=strtok(maskFilenames{i},'_');
    if strcmp(phaseToken,maskToken)==0
       error('scaled mask and scaled phase files dont match up'); 
    end
    
  
    maskLength=length(imfinfo(fullfile(directory,maskFilenames{i})));
    phaseLength=length(imfinfo(fullfile(directory,phaseFilenames{i})));
    if maskLength~=phaseLength
        error(['mask and phase file lengths diffent on files named ' phaseToken ]);
    end
    
end

fprintf('folder looks good \n');


    %% Standard PIV Settings
    s = cell(11,2); % To make it more readable, let's create a "settings table"
    %Parameter                          %Setting           %Options
    s{1,1}= 'Int. area 1';              s{1,2}=128;         % window size of first pass
    s{2,1}= 'Step size 1';              s{2,2}=64;         % step of first pass
    s{3,1}= 'Subpix. finder';           s{3,2}=1;          % 1 = 3point Gauss, 2 = 2D Gauss
    s{4,1}= 'Mask';                     s{4,2}=[];         % If needed, generate via: imagesc(image); [temp,Mask{1,1},Mask{1,2}]=roipoly;
    s{5,1}= 'ROI';                      s{5,2}=[];         % Region of interest: [x,y,width,height] in pixels, may be left empty
    s{6,1}= 'Nr. of passes';            s{6,2}=2;          % 1-4 nr. of passes
    s{7,1}= 'Int. area 2';              s{7,2}=64;         % second pass window size
    s{8,1}= 'Int. area 3';              s{8,2}=16;         % third pass window size
    s{9,1}= 'Int. area 4';              s{9,2}=16;         % fourth pass window size
    s{10,1}='Window deformation';       s{10,2}='*linear'; % '*spline' is more accurate, but slower
    s{11,1}='Repeated Correlation';     s{11,2}=0;         % 0 or 1 : Repeat the correlation four times and multiply the correlation matrices.
    s{12,1}='Disable Autocorrelation';  s{12,2}=0;         % 0 or 1 : Disable Autocorrelation in the first pass. 

    %% Standard image preprocessing settings
    p = cell(8,1);
    %Parameter                       %Setting           %Options
    p{1,1}= 'ROI';                   p{1,2}=s{5,2};     % same as in PIV settings
    p{2,1}= 'CLAHE';                 p{2,2}=1;          % 1 = enable CLAHE (contrast enhancement), 0 = disable
    p{3,1}= 'CLAHE size';            p{3,2}=50;         % CLAHE window size
    p{4,1}= 'Highpass';              p{4,2}=0;          % 1 = enable highpass, 0 = disable
    p{5,1}= 'Highpass size';         p{5,2}=15;         % highpass size
    p{6,1}= 'Clipping';              p{6,2}=0;          % 1 = enable clipping, 0 = disable
    p{7,1}= 'Wiener';                p{7,2}=0;          % 1 = enable Wiener2 adaptive denaoise filter, 0 = disable
    p{8,1}= 'Wiener size';           p{8,2}=3;          % Wiener2 window size
    p{9,1}= 'Minimum intensity';     p{9,2}=0.0;          % Minimum intensity of input image (0 = no change) 
    p{10,1}='Maximum intensity';     p{10,2}=1.0;         % Maximum intensity on input image (1 = no change)

    interpolate=1;  %edit, only interpolate in vector validation if interpolate is set to 1


    %% PIV analysis loop:
 %these are a bunch of PIV outputs that may be useful, tuned for circles in
 %the function PIVpropsEdge written by Matt. 
numberOfStacks=length(maskFilenames);
 u=cell(numberOfStacks,1);
 v=u;
 vMag=u;
 radVel=u;
 ordPar=u;
 tOrdPar=u;
 disFromEdge=u;
 rotation=u;
 divergenceNorm=u;
 divergence=u;
 shearRate=u;
 shearRateNorm=u;
 strainRate=u;
 strainRateNorm=u;
 vorticity=u;
 vorticityNorm=u;

 
 %nested for loop. The outside loop goes cycles through different stacks and the inside
 %loop goes through each timepoint of the current stack
for stackFile=1:numberOfStacks

    % if mod(amount,2) == 1 %Uneven number of images? %edited out
    %     disp('Image folder should contain an even number of images.')
    %     %remove last image from list
    %     amount=amount-1;
    %     filenames(size(filenames,1))=[];
    % end
    numberOfSlicesThisStack=length(imfinfo(fullfile(directory,maskFilenames{stackFile})));%this is just the number of timepoints in this stack
    disp(['Found ' num2str(numberOfSlicesThisStack) ' images (' num2str(numberOfSlicesThisStack-1) ' image pairs).'])
    %x=cell(amount/2,1); %original
    thisX=cell(numberOfSlicesThisStack-1,1);%PIV parameter
    thisY=thisX;%PIV parameter
    thisU=thisX;%U is the x component of the velocity in units pixels/frame
    thisV=thisX;%y component
    typevector=thisX; %typevector will be 1 for regular vectors, 0 for masked areas
    counter=0;
   
    for i=1:1:numberOfSlicesThisStack-1 %changing fom 1:2:amount to 1:1:amount-1 for 1-2,2-3
        counter=counter+1;
        image1=imread(fullfile(directory, phaseFilenames{stackFile}),i); % read images (for stacks, specificy file name, followed by which timepoint sep by comma)
        image2=imread(fullfile(directory, phaseFilenames{stackFile}),i+1);
        image1preproc = PIVlab_preproc (image1,p{1,2},p{2,2},p{3,2},p{4,2},p{5,2},p{6,2},p{7,2},p{8,2},p{9,2},p{10,2}); %preprocess images
        image2preproc = PIVlab_preproc (image2,p{1,2},p{2,2},p{3,2},p{4,2},p{5,2},p{6,2},p{7,2},p{8,2},p{9,2},p{10,2});
        [thisX{counter}, thisY{counter}, thisU{counter}, thisV{counter}, typevector{counter}] = piv_FFTmulti (image1preproc,image2preproc,s{1,2},s{2,2},s{3,2},s{4,2},s{5,2},s{6,2},s{7,2},s{8,2},s{9,2},s{10,2},s{11,2},s{12,2});
        clc
        disp([int2str((i+1)/numberOfSlicesThisStack*100) ' %']);

        % Graphical output (disable to improve speed)
        
%         figure()
%         imagesc(double(image1)+double(image2));colormap('gray');
%         hold on
%         quiver(thisX{counter},thisY{counter},thisU{counter},thisV{counter},'g','AutoScaleFactor', 7);
%         hold off;
%         axis image;
%         title(phaseFilenames{stackFile},'interpreter','none')
%         set(gca,'xtick',[],'ytick',[])
%         drawnow;
%         
    end

     %PIV postprocessing loop
    % Settings
    umin = -100; % minimum allowed u velocity, adjust to your data
    umax = 100; % maximum allowed u velocity, adjust to your data
    vmin = -100; % minimum allowed v velocity, adjust to your data
    vmax = 100; % maximum allowed v velocity, adjust to your data
    stdthresh=3; % threshold for standard deviation check
    epsilon=0.15; % epsilon for normalized median test
    thresh=3; % threshold for normalized median test

    %u_filt=cell(amount/2,1); %commented out for amount

    for PIVresult=1:size(thisX,1)
        u_filtered=thisU{PIVresult,1};
        v_filtered=thisV{PIVresult,1};
        typevector_filtered=typevector{PIVresult,1};
        %vellimit check
        u_filtered(u_filtered<umin)=NaN;
        u_filtered(u_filtered>umax)=NaN;
        v_filtered(v_filtered<vmin)=NaN;
        v_filtered(v_filtered>vmax)=NaN;
        % stddev check
        meanu=nanmean(nanmean(u_filtered));
        meanv=nanmean(nanmean(v_filtered));
        std2u=nanstd(reshape(u_filtered,size(u_filtered,1)*size(u_filtered,2),1));
        std2v=nanstd(reshape(v_filtered,size(v_filtered,1)*size(v_filtered,2),1));
        minvalu=meanu-stdthresh*std2u;
        maxvalu=meanu+stdthresh*std2u;
        minvalv=meanv-stdthresh*std2v;
        maxvalv=meanv+stdthresh*std2v;
        u_filtered(u_filtered<minvalu)=NaN;
        u_filtered(u_filtered>maxvalu)=NaN;
        v_filtered(v_filtered<minvalv)=NaN;
        v_filtered(v_filtered>maxvalv)=NaN;
        % normalized median check
        %Westerweel & Scarano (2005): Universal Outlier detection for PIV data
        [J,I]=size(u_filtered);
        medianres=zeros(J,I);
        normfluct=zeros(J,I,2);
        b=1;
        for c=1:2
            if c==1; velcomp=u_filtered;else;velcomp=v_filtered;end %#ok<*NOSEM>
            for i=1+b:I-b
                for j=1+b:J-b
                    neigh=velcomp(j-b:j+b,i-b:i+b);
                    neighcol=neigh(:);
                    neighcol2=[neighcol(1:(2*b+1)*b+b);neighcol((2*b+1)*b+b+2:end)];
                    med=median(neighcol2);
                    fluct=velcomp(j,i)-med;
                    res=neighcol2-med;
                    medianres=median(abs(res));
                    normfluct(j,i,c)=abs(fluct/(medianres+epsilon));
                end
            end
        end
        info1=(sqrt(normfluct(:,:,1).^2+normfluct(:,:,2).^2)>thresh);
        u_filtered(info1==1)=NaN;
        v_filtered(info1==1)=NaN;

        typevector_filtered(isnan(u_filtered))=2;
        typevector_filtered(isnan(v_filtered))=2;
        typevector_filtered(typevector{PIVresult,1}==0)=0; %restores typevector for mask

        %Interpolate missing data
        if interpolate==1
        u_filtered=inpaint_nans(u_filtered,4);
        v_filtered=inpaint_nans(v_filtered,4);
        end
        thisMask=logical(imread(fullfile(directory, maskFilenames{stackFile}),PIVresult));
        scaledMask=imfill(bwareafilt(imresize(thisMask,size(u_filtered)),1),'holes');
        u_filt_masked=u_filtered;
        u_filt_masked(scaledMask==0)=nan;
        v_filt_masked=v_filtered;
        v_filt_masked(scaledMask==0)=nan;
        u{stackFile,1}(:,:,PIVresult)=u_filt_masked*binAmount*framesPerHour*pixelSizeMicrons;
        v{stackFile,1}(:,:,PIVresult)=v_filt_masked*binAmount*framesPerHour*pixelSizeMicrons;
%         u_filt{PIVresult,1}=u_filtered;
%         v_filt{PIVresult,1}=v_filtered;
    end

    
    %calculates things like shear rate, speed, etc for use later
%     if s{6,2}>1
%      [vMag{stackFile,1},radVel{stackFile,1},ordPar{stackFile,1},disFromEdge{stackFile,1},tOrdPar{stackFile,1},...
%         rotation{stackFile,1},divergenceNorm{stackFile,1},divergence{stackFile,1},strainRate{stackFile,1},strainRateNorm{stackFile,1},...
%         shearRate{stackFile,1},shearRateNorm{stackFile,1},vorticity{stackFile,1},vorticityNorm{stackFile,1}]...
%         =PIVpropsEdge(u{stackFile,1},v{stackFile,1},s{2,2}/s{1,2}*s{5+s{6,2},2}*binAmount,pixelSizeMicrons,rollingAverage);
%     else
%      [vMag{stackFile,1},radVel{stackFile,1},ordPar{stackFile,1},disFromEdge{stackFile,1},tOrdPar{stackFile,1},...
%         rotation{stackFile,1},divergenceNorm{stackFile,1},divergence{stackFile,1},strainRate{stackFile,1},strainRateNorm{stackFile,1},...
%         shearRate{stackFile,1},shearRateNorm{stackFile,1},vorticity{stackFile,1},vorticityNorm{stackFile,1}]...
%         =PIVpropsEdge(u{stackFile,1},v{stackFile,1},s{2,2}*binAmount,pixelSizeMicrons,rollingAverage);
%     end
end

filetokens=strtok(phaseFilenames,'_');

clearvars -except u v directory filetokens vMag radVel disFromEdge ordPar tOrdPar rotation divergence...
    strainRate shearRate vorticity saveName
 
save(saveName);
disp('DONE.')