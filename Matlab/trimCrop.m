function [rectAfterCrop, centroidAfterCrop]=trimCrop(inputMask,inputRect,finalDimension)
prop=regionprops(inputMask,'Centroid');
centroid=prop.Centroid;
xySize=fliplr(size(inputMask));
lrtbToTrim=-round(-0.5+[finalDimension(1)/2-centroid(1),finalDimension(1)/2-(xySize(1)-centroid(1));...
    finalDimension(2)/2-centroid(2),finalDimension(2)/2-(xySize(2)-centroid(2))]);
%trimmed by extra 1 pixel in the line above just to be safe
lrtbToTrim(lrtbToTrim<0)=0;
croppedMask=inputMask(1+lrtbToTrim(2,1):end-lrtbToTrim(2,2),...
    1+lrtbToTrim(1,1):end-lrtbToTrim(1,2));
rectAfterCrop=inputRect+[lrtbToTrim(1,1),lrtbToTrim(2,1),-(sum(lrtbToTrim(1,:))),...
    -sum(lrtbToTrim(2,:))];
%remember, rect is [xmin, ymin, width, height]
prop=regionprops(croppedMask,'Centroid');
centroidAfterCrop=prop.Centroid;
end