function [vMag, radialVel, orderParam, disFromEdge, tangentialOP, rotation, divergenceNorm, divergence, strainRate,strainRateNorm...
    ,shearRate,shearRateNorm,vorticity,vorticityNorm] = PIVpropsEdge(vxInput, vyInput,PIVBoxStepPixels,pixelSizeMicrons,rollingAverage)

if iscell(vxInput)==1
    radius=zeros(1,length(vxInput));
    centroidMap=zeros([size(vxInput{1}),length(vxInput)]);
    vX=nan(size(centroidMap));
    vY=nan(size(centroidMap));
    for i = 1:length(vxInput)
        vX(:,:,i)=vxInput{i};
        vY(:,:,i)=vyInput{i};
    end
    numberOfTsteps=length(vxInput);

else
    radius=zeros(1,size(vxInput,3));
    centroidMap=zeros(size(vxInput));    
    vX=vxInput;
    vY=vyInput;
    numberOfTsteps=size(vxInput,3);
end

[xpixels,ypixels]=meshgrid(1:size(centroidMap,2),1:size(centroidMap,1));

vMag=sqrt(vX.^2+vY.^2);
disFromEdge=zeros(size(vX));
for i=1:numberOfTsteps
    if iscell(vxInput)==1
        %thisInverseBW=~bwareafilt((abs(vxInput{i})>0),1);
        thisInverseBW=isnan(vxInput{i});
    else
        %thisInverseBW=~bwareafilt((abs(vxInput(:,:,i))>0),1);
        thisInverseBW=isnan(vxInput(:,:,i));
    end
    disFromEdge(:,:,i)=bwdist(thisInverseBW);
    if i==1
        props = regionprops(thisInverseBW);
        radius(i)=sqrt(props.Area/pi);
        xC = round(props.Centroid(1));
        yC = round(props.Centroid(2));
    end
end

centroidCenteredXMesh=xpixels-xC;
centroidCenteredYMesh=(ypixels-yC);
meshAngles=atan2(centroidCenteredYMesh,centroidCenteredXMesh);
tangentialMeshAngles=meshAngles+pi/2;

vdir=atan2(vY,vX);
angleDifference=meshAngles-vdir;
tangentialAngleDifference=tangentialMeshAngles-vdir;
orderParam=cos(angleDifference);
tangentialOP=abs(cos(tangentialAngleDifference));
rotation=cos(tangentialAngleDifference);
radialVel=vMag.*orderParam;

%vxZeros=vxInput;
%vyZeros=vyInput;
% vxZeros(isnan(vxZeros))=0;
% vyZeros(isnan(vyZeros))=0;
divergence=nan(size(vxInput));
strainRate=divergence;
shearRate=divergence;
vorticity=divergence;

% divergence(2:end-1,2:end-1)=(vxZeros(2:end-1,3:end)-vxZeros(2:end-1,1:end-2))/(2*pixelSizeMicrons*binAmount*framesPerHour)...
%     +(vyZeros(3:end,2:end-1)-vyZeros(1:end-2,2:end-1))/(2*pixelSizeMicrons*binAmount*framesPerHour);
divergence(2:end-1,2:end-1)=(vxInput(2:end-1,3:end)-vxInput(2:end-1,1:end-2))/(2*pixelSizeMicrons*PIVBoxStepPixels)...
    +(vyInput(3:end,2:end-1)-vyInput(1:end-2,2:end-1))/(2*pixelSizeMicrons*PIVBoxStepPixels);

strainRate(2:end-1,2:end-1)=(vxInput(2:end-1,3:end)-vxInput(2:end-1,1:end-2))/(2*pixelSizeMicrons*PIVBoxStepPixels)...
    -(vyInput(3:end,2:end-1)-vyInput(1:end-2,2:end-1))/(2*pixelSizeMicrons*PIVBoxStepPixels);

shearRate(2:end-1,2:end-1)=(vxInput(3:end,2:end-1)-vxInput(1:end-2,2:end-1))/(2*pixelSizeMicrons*PIVBoxStepPixels)...
    +(vyInput(2:end-1,3:end)-vyInput(2:end-1,1:end-2))/(2*pixelSizeMicrons*PIVBoxStepPixels);

vorticity(2:end-1,2:end-1)=-(vxInput(3:end,2:end-1)-vxInput(1:end-2,2:end-1))/(2*pixelSizeMicrons*PIVBoxStepPixels)...
    +(vyInput(2:end-1,3:end)-vyInput(2:end-1,1:end-2))/(2*pixelSizeMicrons*PIVBoxStepPixels);

%divergence(isnan(vxInput))=nan;

divergenceNorm=divergence./vMag;
strainRateNorm=strainRate./vMag;
shearRateNorm=shearRate./vMag;
vorticityNorm=vorticity./vMag;

if rollingAverage==1
    vMagNorm=nan(size(vMag,1),size(vMag,2),size(vMag,3)-2);
    for i=2:size(vMag,3)-1
        vMagNorm(:,:,i-1)=mean(vMag(:,:,i-1:i+1),3);
    end
    vMag=vMagNorm;
    divergence=divergenceNorm(:,:,2:end-1).*vMag;
    strainRate=strainRateNorm(:,:,2:end-1).*vMag;
    shearRate=shearRateNorm(:,:,2:end-1).*vMag;
    vorticity=vorticityNorm(:,:,2:end-1).*vMag;
    
end


end