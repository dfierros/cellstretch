%Calculates area changes of many mask tif stacks in a folder. You can have
%other tif stacks in this folder as well, just make sure you uniquely
%identify the mask stacks in the name. _mask is fine.

saveName='diegosquare_Dynamics.mat';
directory=uigetdir; %directory containing the images you want to analyze


binAmount=1; %is your data scaled at all? Best practice is to leave it untouched
pixelSize=1.308/1000; %here, pixel size is in millimeters
framesPerHour=3; %frame rate of your tif stack

%pull out all of the tif stacks that refer to mask files you want to
%analyze
maskContains='Mask*.tif'; %*.bmp or *.tif or *.jpg or *.tiff or *.jpeg
maskDirec=dir([directory,filesep,maskContains]); maskFilenames={};
[maskFilenames{1:length(maskDirec),1}] = deal(maskDirec.name);
maskFilenames = sortrows(maskFilenames); %sort all image files


    %% Analysis loop:
numberOfStacks=length(maskFilenames)

%lots of ideas of statistics to pull out
areaMM=cell(numberOfStacks,1);
areaPercentChange=areaMM;
perimeterMM=areaMM;
radiusMM=areaMM;
majorAxisMM=areaMM;
minorAxisMM=areaMM;
aspectRatio=areaMM;
edgeSmoothness=areaMM;
circularity=areaMM;
effectiveVelocity=areaMM;
radiusChangeMM=areaMM;
widthChangeMM=areaMM;
heightChangeMM=areaMM;
widthEffectiveVelocity=areaMM;
heightEffectiveVelocity=areaMM;
widthMM = areaMM;
heightMM = areaMM;

for stackFile=1:numberOfStacks 
    % if mod(amount,2) == 1 %Uneven number of images? %edited out
    %     disp('Image folder should contain an even number of images.')
    %     %remove last image from list
    %     amount=amount-1;
    %     filenames(size(filenames,1))=[];
    % end
    amount=length(imfinfo(fullfile(directory,maskFilenames{stackFile})))
    stackFile

    for i=1:1:amount
       i
        image=logical(imread(fullfile(directory, maskFilenames{stackFile}),i)); % read image, and make sure it is binary
        thisBlob = regionprops(image, 'Area','Perimeter', 'BoundingBox')  ;
        %regionprops is the classical matlab blob analysis function. Lots
        %of useful outputs including areas
        
        areaMM{stackFile}(i)=thisBlob.Area*pixelSize^2*binAmount^2; %area in millimeters
        areaPercentChange{stackFile}(i)=areaMM{stackFile}(i)/areaMM{stackFile}(1)-1;
        perimeterMM{stackFile}(i)=thisBlob.Perimeter*pixelSize*binAmount;
%         radiusMM{stackFile}(i)=thisBlob.EquivDiameter/2*pixelSize*binAmount;
%         majorAxisMM{stackFile}(i)=thisBlob.MajorAxisLength*pixelSize*binAmount;
%         minorAxisMM{stackFile}(i)=thisBlob.MinorAxisLength*pixelSize*binAmount;
       
        circularity{stackFile}(i)=4*pi*areaMM{stackFile}(i)/(perimeterMM{stackFile}(i))^2;
        
        widthMM{stackFile}(i) = thisBlob.BoundingBox(3)*pixelSize;
        heightMM{stackFile}(i) = thisBlob.BoundingBox(4)*pixelSize;
        
        edgeSmoothness{stackFile}(i)=(2*widthMM{stackFile}(i)+2*heightMM{stackFile}(i))/perimeterMM{stackFile}(i);
        aspectRatio{stackFile}(i)=heightMM{stackFile}(i)/widthMM{stackFile}(i);
        
        if i>1.1
            widthEffectiveVelocity{stackFile}(i-1)=(heightMM{stackFile}(i)-heightMM{stackFile}(i-1))*framesPerHour*1000;
            heightEffectiveVelocity{stackFile}(i-1)=(widthMM{stackFile}(i)-widthMM{stackFile}(i-1))*framesPerHour*1000;
%             radiusChangeMM{stackFile}(i-1)=(radiusMM{stackFile}(i)-radiusMM{stackFile}(1));
            widthChangeMM{stackFile}(i-1)=(heightMM{stackFile}(i)-heightMM{stackFile}(1));
            heightChangeMM{stackFile}(i-1)=(widthMM{stackFile}(i)-widthMM{stackFile}(1));

        end
    end

end
filetokens=strtok(maskFilenames,'_');
clearvars -except directory filetokens widthEffectiveVelocity heightEffectiveVelocity...
    areaMM areaPercentChange perimeterMM  heightMM widthMM aspectRatio edgeSmoothness...
    circularity widthChangeMM heightChangeMM saveName
 
save(saveName);
disp('DONE.')