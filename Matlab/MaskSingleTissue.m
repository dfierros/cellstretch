%{
This function takes a cropped image (image with only one tissue), masks it,
and only keeps the largest "blob." In the middle, I pasted in the
automatic thresholding technique of triangle thresholded, as borrowed from
a matlab fileExchange code.
%}



function [maskedImage]=MaskSingleTissue(inputImage)

findEdges=imgradient(inputImage); %equivalen to imageJ find edges
edges8bit=uint8((findEdges/max(max(findEdges)))*(2^8-1));%normalizes the find edges image to 8-bit scale
gaussBlur=imgaussfilt(edges8bit,3); %gaussian blur
[lehisto,~]=imhist(gaussBlur); %"lehisto" is the histogram of the blurred find edges image
% 
% [level]=triangle_th(gaussBlur,256);

%the following is from the file exchange:
num_bins=256;

 
    [~,xmax]=max(lehisto);
    xmax=round(mean(xmax));   %can have more than a single value!
    h=lehisto(xmax);
    
%   Find location of first and last non-zero values.
%   Values<h/10000 are considered zeros.
    indi=find(lehisto>h/10000);
    fnz=indi(1);
    lnz=indi(end);

%   Pick side as side with longer tail. Assume one tail is longer.
    lspan=xmax-fnz;
    rspan=lnz-xmax;
    if rspan>lspan  % then flip lehisto
        lehisto=fliplr(lehisto');
        a=num_bins-lnz+1;
        b=num_bins-xmax+1;
        isflip=1;
    else
        lehisto=lehisto';
        isflip=0;
        a=fnz;
        b=xmax;
    end
    
%   Compute parameters of the straight line from first non-zero to peak
%   To simplify, shift x axis by a (bin number axis)
    m=h/(b-a);
    
%   Compute distances
    x1=0:(b-a);
    y1=lehisto(x1+a);
    beta=y1+x1/m;
    x2=beta/(m+1/m);
    y2=m*x2;
    L=((y2-y1).^2+(x2-x1).^2).^0.5;

%   Obtain threshold as the location of maximum L.    
    level=find(max(L)==L);
    level=a+mean(level);
    
%   Flip back if necessary
    if isflip
        level=num_bins-level+1;
    end
    
    level=level/num_bins;


% end file exchange


BW=imbinarize(gaussBlur,level); %use the triangle threshold level found to make the image a binary image
BWfilt=bwareafilt(BW,1);%only keep the largest blob

%the rest here is because I have found that this method has a systematic
%overshoot of area that can be compensated for with the following:
BWreverse=imcomplement(BWfilt);
edgeDist=bwdist(BWreverse); %edgeDist will be an image where the value is how far the pixel is from the outside of the blob
eroded=edgeDist>9; %kills all pixels within 9 of the edge
erodedfilt=bwareafilt(eroded,1);%just in case, only keep the largest blob once more
erodedfiltfilled=imfill(erodedfilt,'holes');%fill holes
maskedImage=erodedfiltfilled; %done!
end