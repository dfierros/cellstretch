%{
This function takes a cropped image (likely one that has been trimmed to be
within the bounds of your desired final image), places the tissue
centroid in the center of the image, and pads with zeros to fill the image.
Note: this doesn't actually "pad" all around, it just places the original
image in the correct location within an image of zeros that is the correct
size.

%}

function [paddedImage] = padImage(croppedImage, centroid, finalDimension)
xySize=fliplr(size(croppedImage)); %size in [x,y]
LeftTopToAdd=round([finalDimension(1)/2-centroid(1),finalDimension(2)/2-centroid(2)]);
%number of pixels needed to add to the left and top of the matrix,
LeftTopToAdd(LeftTopToAdd<0)=0;
paddedImage=zeros(finalDimension(2),finalDimension(1),class(croppedImage));
%initializes your image as an image of zeros of the same class as the input
%(will be 8bit if input is 8bit, etc.)
paddedImage(1+LeftTopToAdd(2):LeftTopToAdd(2)+xySize(2),...
    LeftTopToAdd(1)+1:LeftTopToAdd(1)+xySize(1))=croppedImage;
%places the input image in the correct location of the zeros image
paddedImage=paddedImage(1:finalDimension(2),1:finalDimension(1));
%safety check just to be sure the final size is correct
end