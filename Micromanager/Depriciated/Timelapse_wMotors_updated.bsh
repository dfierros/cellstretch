// Acquisition script for Long Cai
// Nico Stuurman, Dec. 2010
// Edited by Diego Fierros, March 2019 to include motor movement

import org.micromanager.api.*; // All micromanager API functions
import mmcorej.CharVector;     // Micromanager character vector

import java.lang.System;       // Java system commands (print, etc)
import java.util.Calendar;     // Timing using system time
import java.text.SimpleDateFormat; // Time human readable format

// clear all previous acquisitions
gui.closeAllAcquisitions();
gui.clearMessageWindow();

// file locations
acqName = "PositionList"; // Name of acquisition set
rootDirName = "Y:/Diego/MicroManager_Test"; // Root directory of files

// Capture Parameters
numFrames = 1; // Number of captures at each position
String[] channels = {"Phase"};  // Capture Channel (can add more)
Color[] colors = {Color.WHITE}; // Capture color (match with channel)
int[] exposures = {100};        // Capture exposure (match with channel)
numSlices = 1;                  // Number of z slices
intervalFrame = 100;            // ms interval between frames

numInMontage = 9;               // Specify number of images in each montage 
intervalCaptureMin = 1;         // Interval between caputures in minutes
intervalCaptureMs = intervalCaptureMin * 60000;
timePoints = 3;                 // Number of captures to make

// Serial config
port = "COM20";
timeout = 20; // timeout in # of attempts for serial connections

handshake_msg = "HS0\n";      // Handshake message
handshake_expRes = "HS1\n";   // Expected handshake response
move_expRes = "MVC\n";      // Expected motor movement response

// Time
SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

// Get Position List from GUI
PositionList pl = gui.getPositionList();
String[] acqs = new String[pl.getNumberOfPositions()];

// Send a message (msg) to the arduino with expected response (expRes)
sendToArduino(msg, expRes) {
    serial_response = "BLANK";

    // Change input message into character vector
    CharVector send = new CharVector(); 
    for (int i = 0; i < msg.length(); i++) {
        send.add(msg.charAt(i));
    }

    // Write command to serial port 
    mmc.writeToSerialPort(port, send);

    // Check for response (timeout) times
    for (i = 0; i < timeout; i++) {
        answer = mmc.readFromSerialPort(port);

        if (answer.capacity() > 0) { // If answer recieved
            for (i = 0; i < answer.capacity(); i++) {
                if (serial_response == "BLANK") {
                    serial_response = "" + answer.get(i);
                }   // have to add the "" otherwise adds decimal value
                else {
                    serial_response = "" + serial_response +(char)answer.get(i);
                }
            }

            print(serial_response); // Debug statement
            
            // Check whether or not response is equal to expected response
            if (serial_response.equals(expRes)) {
				return 1; // Ends function with success
			}
			else {
				return 0; // Ends function with failure (may want to remove)
            }
        }

        print("Serial command: " + msg + " Awaiting Response...(attempt " + i + ")");
        Thread.sleep(400); // Wait 400 ms (to avoid spamming)
    }
}

// BEGIN CAPTURE PROGRAM LOOP --------------------------------------------------

checkMontage = pl.getNumberOfPositions() % numInMontage;

try { 
    if (checkMontage > 0)
        throw new IllegalArgumentException("Incompatible montage size"); 
} 
catch(IllegalArgumentException e) 
{ 
    System.out.println("TODO: prompt user to change montage settings"); 
    throw e; // rethrowing the exception 
} 

numMontages = pl.getNumberOfPositions() / numInMontage;

for (int i = 0; i < numMontages; i++) {
    acqs[i] = "tissue_" + i; // Mark tissue number

    // Open an aquisiiton for each tissue
    gui.openAcquisition(acqs[i], rootDirName, numFrames,
    channels.length, numSlices, numInMontage, true, true);
    
    // Set channel colors and names for each acquisition
    for (int j = 0; j < colors.length; j++)
        gui.setChannelColor(acqs[i], j, colors[j]);
    for (int j = 0; j < channels.length; j++)
        gui.setChannelName(acqs[i], j, channels[j]);
}

// Set channel settings and open acquisition
// Main System Loop
for (int tp = 0; tp < timePoints; tp++) { // Excecute over (tp) timepoints

    for (int i = 0; i < pl.getNumberOfPositions(); i++) {
        tissueNum = i / numInMontage;

        MultiStagePosition.goToPosition(pl.getPosition(i), mmc);
        gui.message("XY Stage moving to point: " + pl.getPosition(i).getLabel());
        core.waitForDevice("XYStage");

        gui.message("Acquiring " + acqs[i]);
        
        // Capture each channel
        for (int j = 0; j < channels.length; j++) {
            mmc.setExposure(exposures[j]);
	   		mmc.setConfig(channelGroup, channels[j]);
            mmc.waitForConfig(channelGroup, channels[j]);
            
            // Capture each frame
            for (int k = 0; k < numFrames; k++) {
                now = System.currentTimeMillis();
                gui.message("Acquiring: " + acqs[i] + " Frame: " + k + " Channel: " + channels[j]);
                
                // Add image to acquision
                // Z slice currently locked to zero
                gui.snapAndAddImage(acqs[tissueNum], tp, j, 0, i % numInMontage);
                
                // Set contrast based on first frame
                if (k == 0)
                    gui.setContrastBasedOnFrame(acqs[tissueNum], k, 0);
                
                // If taking multiple caputres at each position, wait
                itTook = System.currentTimeMillis() - now;
				if (numFrames > 1 && itTook < intervalFrame)
                    gui.sleep(intervalFrame - itTook);
            }
        }

        // Note the time
        Calendar cal = Calendar.getInstance();
	    gui.message("Timepoint " + tp + " capture complete -- > " + sdf.format(cal.getTime()));
	    gui.message("Moving Motors...");

        // Handshake with arduino
        shake = sendToArduino(handshake_msg, handshake_expRes);
        
        if (shake == 1) {
            sendToArduino('MV+\n', move_expRes); // Tesnsion tissue 1mm
            // sendToArduino('MV-\n', move_expRes); // Compress tissue 1mm
        } else {
            print("ERROR: Arduino Handshake failed"); //TODO: add actual error
        }
        
        // Reset timepoint and wait until next capture
        cal = Calendar.getInstance();
	    cal.add(cal.MINUTE, intervalCaptureMin);
	    gui.message("Next Capture -- > " + sdf.format(cal.getTime()));
        gui.sleep(intervalCaptureMs);
    }