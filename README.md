![alt text](complete_stage.png)

# Cell Strecher

This is the official thesis repository of Diego Fierros '19 for the Cohen Group. Included in the repository are CAD models, simulation code, and documentation pertaining to the design, construction, and programmatic control of the Zeiss Cell Strethcing System

## Getting Started

Simply clone this repository by running

```
git clone https://dfierros@bitbucket.org/dfierros/cellstretch.git
```
in your git bash to get started!

### Glossary

* Membrane: Bisco HT 2040 that has been cut by the Sillouhette razor writer.
* Tissue (With regards to membranes): a shaped group of cells at or around confluence that have been cultured on the Bisco membrane

### Repository Organization

The readme will follow a similar organization to the repository, which is laid out below


* [Arduino_Scripts](#ArduinoScripts)
* [CAD](#CAD)        
* [Comsol](#Comsol)          
* [Data](#Data)           
* [Hardware](#Hardware)     
* [ImageJ](#ImageJ)         
* [Matlab](#Matlab)         
* [Micromanager](#Micromanager)    
    * [Depriciated](##Depriciated)
    * [Examples](##Examples)
* [stretch-app](#stretch-app) 

Click on any of the above links to reach the specified section

# Arduino_Scripts
This folder contains Arduino scripts to control 

Arduino scripts required to control the stretcher motors and talk to micromanager to schedule movements.

# CAD
CAD parts and assemblies relevant to the stretcher. There are also models of lab equipment such as 10cm dishes and the Zeiss stage mount.

# Comsol
Simulations performed on cell stretching membranes to discern and design strain fields

# Data
A reasonable subset of experimental data that demonstrates the capabilities of the stretcher 

# Hardware
Sensors and actuators used in the stretcher along with wiring diagrams and datasheets.

# ImageJ
Fuji macros for stitching images, creating masks, etc

# Matlab
Matlab scripts for pre-processing and post processing scope images.

# Micromanager
Micromanager is open-source microscopy software used for programmatic control of the Zeiss. This directory contains scripts for performing timelapses with the motorized stage.

## Depriciated
These scripts have been updated/changed and have been included for reference.

## Examples
These scripts are examples from the micromanager website taht are good points of reference for specific micromanager capabilities.

# stretch-app
A work-in-progress electron application for the stretching device


This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Dr. Daniel Cohen, Matt Heinrich, Julie LaChance, Tom Zajdel, and everyone else in the Lab for showing me how cool science can be! 

