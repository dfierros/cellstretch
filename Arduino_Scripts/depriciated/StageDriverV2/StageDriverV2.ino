// Shows how to run three Steppers at once with varying speeds
//
// Requires the Adafruit_Motorshield v2 library 
//   https://github.com/adafruit/Adafruit_Motor_Shield_V2_Library
// And AccelStepper with AFMotor support 
//   https://github.com/adafruit/AccelStepper

// This tutorial is for Adafruit Motorshield v2 only!
// Will not work with v1 shields

#include <Wire.h>
#include <AccelStepper.h>
#include <Adafruit_MotorShield.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

// Connect two steppers with 200 steps per revolution (1.8 degree)
// to the top shield
Adafruit_StepperMotor *myStepper1 = AFMS.getStepper(200, 2);
Adafruit_StepperMotor *myStepper2 = AFMS.getStepper(200, 1);

// Print the help
void helpPrint() {
  Serial.println("--- Command list: ---");
  Serial.println("c #-> Compress Tissue by # mms");  
  Serial.println("s #-> Stretch Tissue by # mms");  
  Serial.println("x -> Home Stretcher");
  Serial.println("h -> Print Help");
}

// you can change these to DOUBLE or INTERLEAVE or MICROSTEP!
// wrappers for the first motor!
void forwardstep1() {  
  myStepper1->onestep(FORWARD, MICROSTEP);
}
void backwardstep1() {  
  myStepper1->onestep(BACKWARD, MICROSTEP);
}
// wrappers for the second motor!
void forwardstep2() {  
  myStepper2->onestep(FORWARD, MICROSTEP);
}
void backwardstep2() {  
  myStepper2->onestep(BACKWARD, MICROSTEP);
}

// Wrap the 3 steppers in an AccelStepper object
AccelStepper stepper1(forwardstep1, backwardstep1);
AccelStepper stepper2(forwardstep2, backwardstep2);

struct pos
{
   int step1pos;
   int step2pos;
};

void setup()
{  
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.setTimeout(100);
  Serial.println("Automated Stretcher Stepper Motors");
  
  AFMS.begin(); // Start the shield
   
  stepper1.setMaxSpeed(200.0);
  stepper1.setAcceleration(100.0);
    
  stepper2.setMaxSpeed(200.0);
  stepper2.setAcceleration(100.0);
  
  Serial.println("Awaiting commands:");
  Serial.println("Enter S to stretch or C to compress followed by a distance in mm");
  Serial.println("Example: S 100");
}

int currentStep = 0;
const float MM_PER_STEP = 0.00031;
int distMM = 0;

int mm2steps(int mm){
  return mm/MM_PER_STEP;
}

// Check to see if the system is currently in motion
bool isSystemRunning(){
  Serial.println(stepper1.isRunning());
  Serial.println(stepper2.isRunning());
  return (stepper1.isRunning() || stepper2.isRunning());
}

struct pos checkCurrentPos() {
  pos returnMe;

  returnMe.step1pos = stepper1.currentPosition();
  returnMe.step2pos = stepper2.currentPosition();
   
  return returnMe;
}

void test(){
  bool running = isSystemRunning();

  Serial.println("Checking if System is Running:"); 
  Serial.println(String(running)); 
  
  pos currentPos = checkCurrentPos();
  Serial.println("Current Motor Positions:"); 
  Serial.println(String("Stepper 1: " + String(currentPos.step1pos))); 
  Serial.println(String("Stepper 2: " + String(currentPos.step2pos))); 
}

bool isReady(){
  return (stepper1.distanceToGo() == 0 && stepper2.distanceToGo() == 0);
}

void stretch(int dist){
  int goal = dist/2;
  
  Serial.println(String("Recieved request to stretch " + String(distMM) + " mm"));
  
  stepper1.moveTo(stepper1.currentPosition() - goal);
  stepper2.moveTo(stepper2.currentPosition() + goal);
}

void compress(int dist){
  int goal = dist/2;

  Serial.println(String("Recieved request to compress " + String(distMM) + " mm"));
  
  stepper1.moveTo(stepper1.currentPosition() + goal);
  stepper2.moveTo(stepper2.currentPosition() - goal);
}

void runMotors(){
  Serial.println("Running motors: " + String(stepper1.distanceToGo()) + " steps");
  while (stepper1.distanceToGo() != 0 && stepper2.distanceToGo() != 0){
    stepper1.run();
    stepper2.run();
  }
  Serial.println("Exiting runloop");
  Serial.println("Current positions:");
  Serial.println("Stepper 1 --> " + String(stepper1.currentPosition()));
  Serial.println("Stepper 2 --> " + String(stepper2.currentPosition()));
}

//    if (isReady()){
//      stretch(1000); 
//    }
//    if (stepper1.distanceToGo() == 0)
//      stepper1.moveTo(0);
//    if (stepper2.distanceToGo() == 0)
//      stepper2.moveTo(100);

char rxChar;
int distSteps;

void loop()
{   
  if (Serial.available() > 0) {
    // read the incoming byte:
    rxChar = Serial.read();            // Save character received. 
//    Serial.flush();                    // Clear receive buffer.

    switch (rxChar) {
      case 's':
      case 'S':
        distMM = Serial.parseInt();
        // Translate mm to step input
        distSteps = mm2steps(distMM);
        stretch(distSteps);
        runMotors();
        break;
      case 'c':
      case 'C':
        distMM = Serial.parseInt();
        // Translate mm to step input
        distSteps = mm2steps(distMM);
        compress(distSteps);
        runMotors();
        break;
      case 'x':
      case 'X':
        Serial.println("Not Implemented");
        break;
      case 'h':
      case 'H':
        Serial.println();
        helpPrint();
        break;
      default:
        Serial.println("ERROR: Invalid input recieved");
        break;
    }
    Serial.parseInt();
    Serial.flush();
  }
}


