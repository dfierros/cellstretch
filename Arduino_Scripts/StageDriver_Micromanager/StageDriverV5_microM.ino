// Requires the Adafruit_Motorshield v2 library 
//   https://github.com/adafruit/Adafruit_Motor_Shield_V2_Library
// And AccelStepper with AFMotor support 
//   https://github.com/adafruit/AccelStepper

// Arduino Code for Running the Tissue stretcher with micromanager
// This version does not return human readable output, but rather
// uses serial handshakes to schedule stretching routines

#include <Wire.h>
#include <AccelStepper.h>
#include <Adafruit_MotorShield.h>

// VARIABLES -- CHANGE ME -----------------------------------------------------

// distace in mm to stretch in each stretch command
const int STRETCH_VAL = 1;

// distance in mm to compress in each compress command
const int COMPRESS_VAL = 1;

// ----------------------------------------------------------------------------

// IMPORTANT -- PROBABLY SHOULD NOT CHANGE ------------------------------------
int currentStep = 0;              // Current step value
const float MM_PER_STEP = 0.00031; // MM per motor step
double distMM = 0.0;              // Distance traveled in MM

// STEPPER MOTOR SETUP -------------------------------------

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

// Connect two steppers with 200 steps per revolution (1.8 degree)
// to the top shield
Adafruit_StepperMotor *myStepper1 = AFMS.getStepper(200, 2);
Adafruit_StepperMotor *myStepper2 = AFMS.getStepper(200, 1);

// you can change these to DOUBLE or INTERLEAVE or MICROSTEP!
// wrapper functions for stepper motor 1
void forwardstep1() {  
  myStepper1->onestep(FORWARD, MICROSTEP);
}
void backwardstep1() {  
  myStepper1->onestep(BACKWARD, MICROSTEP);
}
// wrapper functions for stepper motor 2
void forwardstep2() {  
  myStepper2->onestep(FORWARD, MICROSTEP);
}
void backwardstep2() {  
  myStepper2->onestep(BACKWARD, MICROSTEP);
}

// Wrap the steppers in an AccelStepper object
AccelStepper stepper1(forwardstep1, backwardstep1);
AccelStepper stepper2(forwardstep2, backwardstep2);

// Container struct for position
struct pos
{
   int step1pos;
   int step2pos;
};

void setup()
{  
  Serial.begin(9600);           // set up Serial library at 9600 bps
//  Serial.setTimeout(100);
//  Serial.println("Automated Stretcher Stepper Motors");
  
  AFMS.begin(); // Start the shield
   
  stepper1.setMaxSpeed(200.0);
  stepper1.setAcceleration(100.0);
    
  stepper2.setMaxSpeed(200.0);
  stepper2.setAcceleration(100.0);
}

int mm2steps(double mm){
  return mm/(2*MM_PER_STEP);
}

double steps2mm(double steps){
  return (2*MM_PER_STEP)*steps;
}

// Check to see if the system is currently in motion
bool isSystemRunning(){
  return (stepper1.isRunning() || stepper2.isRunning());
}

struct pos checkCurrentPos() {
  pos returnMe;

  returnMe.step1pos = stepper1.currentPosition();
  returnMe.step2pos = stepper2.currentPosition();
   
  return returnMe;
}

bool isReady(){
  return (stepper1.distanceToGo() == 0 && stepper2.distanceToGo() == 0);
}

void stretch(double dist){
  double goal = dist/2;
  
  stepper1.moveTo(stepper1.currentPosition() - goal);
  stepper2.moveTo(stepper2.currentPosition() + goal);
}

void compress(double dist){
  double goal = dist/2;
  
  stepper1.moveTo(stepper1.currentPosition() + goal);
  stepper2.moveTo(stepper2.currentPosition() - goal);
}

void homeStepp(){  
  stepper1.moveTo(0);
  stepper2.moveTo(0);
}


const int leftHome = A0;  // Analog input pin that the Left Limit is attached to
const int rightHome = A1;  // Analog input pin that the Right Limit is attached to

void runMotors(){
  int h1V = 0;        // value read from left 
  int h2V = 0;        // value read from right
  
  while (stepper1.distanceToGo() != 0 && stepper2.distanceToGo() != 0){
    h1V = analogRead(leftHome);
    h2V = analogRead(rightHome);

    if (h1V == 0 && h2V == 0) {
      stepper1.run();
      stepper2.run();
    }
  }
  
  double totalDist = abs(steps2mm(stepper1.currentPosition())) + abs(steps2mm(stepper2.currentPosition()));
}

boolean newData = false;
String hsToken = "HS0\n";
String mvToken = "MV";

String hsResponse = "HS1\n";
String mvResponse = "MVC\n";

const byte numChars = 10;
char receivedChars[numChars];   // an array to store the received data

void recvWithEndMarker() {
    static byte ndx = 0;
    char endMarker = '\n';
    char rc;
   
    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (rc != endMarker) {
            receivedChars[ndx] = rc;
            ndx++;
            if (ndx >= numChars) {
                ndx = numChars - 1;
            }
        }
        else {
            receivedChars[ndx] = '\0'; // terminate the string
            ndx = 0;
            newData = true;
        }
    }
}

void loop( ){   
    recvWithEndMarker();

    if (hsToken.charAt(0) == receivedChars[0] && hsToken.charAt(1) == receivedChars[1]) {
      if (newData)
        Serial.print(hsResponse);
    } else if (mvToken.charAt(0) == receivedChars[0] && mvToken.charAt(1) == receivedChars[1]){
      if (newData) {
        switch(receivedChars[2]){
          case '+':
          stretch(mm2steps(STRETCH_VAL));
          runMotors();
          break;
    
          case '-':
          compress(mm2steps(COMPRESS_VAL));
          runMotors();
          break;
    
          case 'H':
          case 'h':
          homeStepp();
          runMotors();
          break;
    
          default:
          Serial.println("ERROR: Invalid input recieved");
         }
       Serial.print(mvResponse);
    }
  }

  newData = false;
}
