# Arduino_Scripts

This directory contains Arduino code for programming and running the tissue stretching platform

## Getting Started

To run these scripts, you will need the following hardware:

* An Arduino Uno
    * Requires a [USB A/B Cable](https://store.arduino.cc/usa/usb-2-0-cable-type-a-b) for control
    * MAY require a [12V power supply](https://www.adafruit.com/product/798?gclid=EAIaIQobChMI_7f8qv2n4gIVip6zCh0z1gg1EAQYASABEgJpKfD_BwE) for additional motor power (12V may be sufficient in low strain cases)
* An [Adafruit motorshield v2](https://www.adafruit.com/product/1438)
* Two bipolar stepper motors rated at 1.8 degrees per step (can modify this, but code must be changed)

The motors should be set to the desired starting position BEFORE plugging in the Arduino. Currently, the system operates in open loop control, so it is only capable of moving relative to this initial position.

### Required Libraries

* [Adafruit Motorshield Library](https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/install-software)
* [Accelstepper](https://github.com/adafruit/AccelStepper/archive/master.zip)

## Scripts

* **StageDriver_HumanReadable**: The core functionality of the stretching device made to be used through the Arduino serial monitor. Send "?" to the Ardunio to see a list of available commands

* **StageDriver_Micromanager**: A script for the stretching device made to function with simple serial commands, works with any of the Micromanager Timelapse*.bsh files. (SHOULD BE EDITED BEFORE USING)

* **Depriciated**: Multiple testing scripts and earlier versions of the two final scripts. Should be used with caution, as hardware may have changed.

## Future Work:

It would be good to have limit switches or a closed-loop actuation method to prevent motor crashes and make movement more reliable. 

