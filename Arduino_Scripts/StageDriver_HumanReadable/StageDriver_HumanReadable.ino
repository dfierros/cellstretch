// Shows how to run three Steppers at once with varying speeds
//
// Requires the Adafruit_Motorshield v2 library 
//   https://github.com/adafruit/Adafruit_Motor_Shield_V2_Library
// And AccelStepper with AFMotor support 
//   https://github.com/adafruit/AccelStepper

// This tutorial is for Adafruit Motorshield v2 only!
// Will not work with v1 shields

#include <Wire.h>
#include <AccelStepper.h>
#include <Adafruit_MotorShield.h>

// IMPORTANT VARIABLES -- PROBABLY SHOULD NOT CHANGE ------
int currentStep = 0;              // Current step value
const float MM_PER_STEP = 0.00031; // MM per motor step
double distMM = 0.0;              // Distance traveled in MM

// STEPPER MOTOR SETUP -------------------------------------

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

// Connect two steppers with 200 steps per revolution (1.8 degree)
// to the top shield
Adafruit_StepperMotor *myStepper1 = AFMS.getStepper(200, 2);
Adafruit_StepperMotor *myStepper2 = AFMS.getStepper(200, 1);

// you can change these to DOUBLE or INTERLEAVE or MICROSTEP!
// wrapper functions for stepper motor 1
void forwardstep1() {  
  myStepper1->onestep(FORWARD, MICROSTEP);
}
void backwardstep1() {  
  myStepper1->onestep(BACKWARD, MICROSTEP);
}
// wrapper functions for stepper motor 2
void forwardstep2() {  
  myStepper2->onestep(FORWARD, MICROSTEP);
}
void backwardstep2() {  
  myStepper2->onestep(BACKWARD, MICROSTEP);
}

// Wrap the steppers in an AccelStepper object
AccelStepper stepper1(forwardstep1, backwardstep1);
AccelStepper stepper2(forwardstep2, backwardstep2);

// Container struct for position
struct pos
{
   int step1pos;
   int step2pos;
};

void setup()
{  
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.setTimeout(100);
  Serial.println("Automated Stretcher Stepper Motors");
  
  AFMS.begin(); // Start the shield
   
  stepper1.setMaxSpeed(200.0);
  stepper1.setAcceleration(100.0);
    
  stepper2.setMaxSpeed(200.0);
  stepper2.setAcceleration(100.0);
  
  Serial.println("Awaiting commands:");
  Serial.println("Enter '?' for help");
}

int mm2steps(double mm){
  return mm/(2*MM_PER_STEP);
}

double steps2mm(double steps){
  return (2*MM_PER_STEP)*steps;
}

// Check to see if the system is currently in motion
bool isSystemRunning(){
  Serial.println(stepper1.isRunning());
  Serial.println(stepper2.isRunning());
  return (stepper1.isRunning() || stepper2.isRunning());
}

struct pos checkCurrentPos() {
  pos returnMe;

  returnMe.step1pos = stepper1.currentPosition();
  returnMe.step2pos = stepper2.currentPosition();
   
  return returnMe;
}

void test(){
  bool running = isSystemRunning();

  Serial.println("Checking if System is Running:"); 
  Serial.println(String(running)); 
  
  pos currentPos = checkCurrentPos();
  Serial.println("Current Motor Positions:"); 
  Serial.println(String("Stepper 1: " + String(currentPos.step1pos))); 
  Serial.println(String("Stepper 2: " + String(currentPos.step2pos))); 
}

bool isReady(){
  return (stepper1.distanceToGo() == 0 && stepper2.distanceToGo() == 0);
}

void stretch(double dist){
  double goal = dist/2;
  
  Serial.println(String("Recieved request to stretch " + String(distMM) + " mm"));
  
  stepper1.moveTo(stepper1.currentPosition() - goal);
  stepper2.moveTo(stepper2.currentPosition() + goal);
}

void compress(double dist){
  double goal = dist/2;

  Serial.println(String("Recieved request to compress " + String(distMM) + " mm"));
  
  stepper1.moveTo(stepper1.currentPosition() + goal);
  stepper2.moveTo(stepper2.currentPosition() - goal);
}

void homeStepp(){
  Serial.println(String("Recieved request to home steppers"));
  
  stepper1.moveTo(0);
  stepper2.moveTo(0);
}

void runMotors(){
  Serial.println("Running motors: " + String(steps2mm(stepper1.distanceToGo())) + " mm (" + String(stepper1.distanceToGo()) + " steps)");
  while (stepper1.distanceToGo() != 0 && stepper2.distanceToGo() != 0){
    stepper1.run();
    stepper2.run();
  }
  Serial.println("Exiting runloop");
  Serial.println("Current positions:");
  Serial.println("Stepper 1 --> " + String(steps2mm(stepper1.currentPosition())) + " mm (" + String(stepper1.currentPosition()) + " steps)");
  Serial.println("Stepper 2 --> " + String(steps2mm(stepper2.currentPosition())) + " mm (" + String(stepper2.currentPosition()) + " steps)");

  double totalDist = abs(steps2mm(stepper1.currentPosition())) + abs(steps2mm(stepper2.currentPosition()));
  Serial.println("Total distance from home: " + String(totalDist) + " mm");
}

// Print the help
void helpPrint() {
  Serial.println("--- Command list: ---");
  Serial.println("+ -> Stretch Tissue by 1 mm");  
  Serial.println("- -> Compress Tissue by 2 mm");  
  Serial.println("s #-> Stretch Tissue by # mms");  
  Serial.println("c #-> Compress Tissue by # mms");  
  Serial.println("h -> Home Stretcher")
  Serial.println("? -> Print Help");
}

//    if (isReady()){
//      stretch(1000); 
//    }
//    if (stepper1.distanceToGo() == 0)
//      stepper1.moveTo(0);
//    if (stepper2.distanceToGo() == 0)
//      stepper2.moveTo(100);


int incomingByte = 0;   // for incoming serial dat


void loop()
{   
  if (Serial.available() > 0) {
    // read the incoming byte:
    
    char command = (char)Serial.read();
    distMM = Serial.parseFloat();
    Serial.parseInt();
    Serial.flush();
  
    // say what you got:
//    Serial.println("I received: ");
//    Serial.println(command);
//    Serial.println(dist);

    // Translate mm to step input
    int distSteps = mm2steps(distMM);

    switch(command){

      case '+':
      stretch(mm2steps(1));
      runMotors();
      break;

      case '-':
      compress(mm2steps(1));
      runMotors();
      break;
      
      case 'S':
      case 's':
      stretch(distSteps);
      runMotors();
      break;

      case 'C':
      case 'c':
      compress(distSteps);
      runMotors();
      break;

      case 'H':
      case 'h':
      homeStepp();
      runMotors();
      break;

      case '?':
      helpPrint();
      break;

      default:
      Serial.println("ERROR: Invalid input recieved");
    }
    
  }
}


